
const express = require('express')
const db = require('../db')
const utils = require('../utills')

const router = express.Router()

router.get('/',(request,response)=>{
   const query = `select * from Emp`
   db.execute(query,(error,result)=>{
       response.send(utils.createResult(error,result))
   })
})

router.post('/add',(request,response)=>{
    const{name, salary, age} = request.body
    const query = `insert into Emp
    ( name,salary,age)
  values
    ('${name}',${salary},${age})`

    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})

router.put('/:id',(request,response)=>{
    const{id} = request.params
    const{salary} = request.body
    const query = `update Emp set salary = ${salary} where id = ${id}`

    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})

router.delete('/delete/:id',(request,response)=>{
    const{id} = request.params
    const query = `delete from Emp where id = ${id}`

    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})

module.exports = router